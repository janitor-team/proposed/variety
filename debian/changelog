variety (0.8.7-1) unstable; urgency=medium

  * New upstream release:
    - Allow disabling online access in preferences (Closes: #931728)
  * Packaging changes:
    - Add gbp.conf configuration
    - Drop 0002-Change-default-font-to-Sans-for-compatibility.patch;
      applied upstream
    - Update to Standards-Version 4.6.0, no changes needed

 -- James Lu <james@overdrivenetworks.com>  Sun, 01 May 2022 17:34:12 -0700

variety (0.8.5-2) unstable; urgency=medium

  * Add patch 0002-Change-default-font-to-Sans-for-compatibility.patch:
      Use "Sans" as default font for Quotes and Clock, as Ubuntu and
      Bitstream fonts are not installed by default everywhere. (LP: #1879002)

 -- James Lu <james@overdrivenetworks.com>  Mon, 14 Feb 2022 21:52:11 -0800

variety (0.8.5-1) unstable; urgency=medium

  * New upstream release.
    - Fix preferences window and menus crashing on Python 3.9 due to
      ElementTree.getiterator() removal
  * Refresh the manual page for variety, via help2man

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- James Lu <james@overdrivenetworks.com>  Thu, 10 Dec 2020 00:22:27 -0800

variety (0.8.4-1) unstable; urgency=medium

  * New upstream release.
    - Fix deprecated "is" comparison with literal (Closes: #961260)
    - Remove broken Desktoppr, ChromeOS sources (LP: #1863615)
  * Update to Standards-Version 4.5.0, no changes needed

 -- James Lu <james@overdrivenetworks.com>  Sun, 19 Jul 2020 22:10:29 -0700

variety (0.8.3-1) unstable; urgency=medium

  * New upstream release.

 -- James Lu <james@overdrivenetworks.com>  Wed, 26 Feb 2020 22:10:23 -0800

variety (0.8.2-1) unstable; urgency=medium

  * New upstream release.
  * Remove outdated patches applied upstream:
    - 0002-Update-Wallhaven-API-URL-180.patch
    - 0003-Add-a-Privacy-Notice-dialog-to-warn-about-data-colle.patch
    - 0004-set_wallpaper-always-exit-with-error-code-0.patch
  * Refresh patch remove-outdated-versions-quit.patch
  * d/copyright:
    - Remove unused BSD-3-Clause license text. The last code that used this
      was FacebookHelper, which was dropped in 0.7.0
    - Update copyright years & add copyright section for appdata.xml file
  * d/control:
    - Update to Standards-Version 4.4.1; add Rules-Requires-Root: no
    - Update to debhelper 12.

 -- James Lu <james@overdrivenetworks.com>  Sat, 11 Jan 2020 20:04:07 -0800

variety (0.7.2-2) unstable; urgency=high

  * Update 0003-Add-a-Privacy-Notice-dialog-to-warn-about-data-colle.patch:
    Fix a bug where the first run file is never written, causing the dialogs
    to show up with every run.
    This was reported upstream as
    https://github.com/varietywalls/variety/issues/227

 -- James Lu <james@overdrivenetworks.com>  Sun, 13 Oct 2019 09:48:18 -0700

variety (0.7.2-1) unstable; urgency=medium

  * New upstream release.
  * patches:
    - Drop fix-spurious-error-when-analyzing-gifs.patch,
           fix-crash-on-help-version.patch; applied upstream
    - Add 0002-Update-Wallhaven-API-URL-180.patch:
        Update Wallhaven API URL to fix downloads
    - Add 0003-Add-a-Privacy-Notice-dialog-to-warn-about-data-colle.patch:
        Backport patch adding a privacy policy dialog to Variety's first start.
        This is a stop-gap fix for bug #931728 in the absence of a proper
        offline mode in Variety.
        See https://github.com/varietywalls/variety/issues/198 for
        justification on why data is collected.
    - Add 0004-set_wallpaper-always-exit-with-error-code-0.patch:
        Suppress non-zero exit codes from set_wallpaper subprocesses, as these
        are non-fatal and otherwise fill up Variety's logs with tracebacks.

 -- James Lu <james@overdrivenetworks.com>  Fri, 23 Aug 2019 15:56:17 -0700

variety (0.7.1-2) unstable; urgency=medium

  * Backport bugfixes from Variety 0.7.2:
    - fix-crash-on-help-version.patch: Don't forward --help or --version to
      running Variety instances, as this causes it to crash.
    - fix-spurious-error-when-analyzing-gifs.patch: Fix spurious
      FileNotFoundError when analyzing GIFs inside a wallpaper folder

 -- James Lu <james@overdrivenetworks.com>  Sun, 21 Apr 2019 19:10:58 -0700

variety (0.7.1-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ James Lu ]
  * New upstream release.

 -- James Lu <james@overdrivenetworks.com>  Sun, 28 Oct 2018 20:45:18 -0700

variety (0.7.0-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.

 -- James Lu <james@overdrivenetworks.com>  Sat, 15 Sep 2018 22:28:39 -0700

variety (0.7.0~beta1-1) experimental; urgency=medium

  * New upstream beta release.
    - Port to Python 3: switch to using pybuild and update dependencies to
      their Python 3 equivalents
    - Removed outdated Facebook publishing code
  * Merge upstream packaging changes:
    - Remove R: libglib2.0-bin | trash-cli | konqueror, no longer needed
    - Remove pycurl dependency as the last feature using it (Facebook
      publishing) was taken out
    - Remove runtime dependencies from build-deps since tests are not enabled
      yet (many of them require network access)
  * Update debian/watch to mangle pre-release versions
  * d/control:
    - Bump Standards-Version to 4.2.0, no changes needed
    - Remove S: gnome-shell-extension-top-icons-plus, dead upstream
    - Resync long description with upstream README

 -- James Lu <james@overdrivenetworks.com>  Fri, 17 Aug 2018 14:21:03 -0700

variety (0.6.9-1) unstable; urgency=medium

  * New upstream release (Closes: #898641)
    - Skip metadata updates on non-images, suppressing
      "GExiv2: unsupported format" warnings (Closes: #893759)
  * Merge upstream packaging changes:
    - Recommend libglib2.0-bin | trash-cli | konqueror for Delete to Trash
      feature
    - Drop script-not-executable Lintian override as the relevant code
      was removed in 0.6.9.
  * debian/ folder changes:
    - Bump Standards-Version to 4.1.4, and remove get-orig-source from
      debian/rules accordingly
    - Drop disable-ssl-dep-installation.patch, superseded by upstream changes
    - d/control: remove redundant XS-Python-Version: 2.7 tag

 -- James Lu <james@overdrivenetworks.com>  Sun, 20 May 2018 11:39:04 -0700

variety (0.6.7-1) unstable; urgency=medium

  * New upstream release.
  * Merge upstream packaging changes (which is now mostly synced with Debian):
    - i3 and openbox code now uses either feh or nitrogen interchangeably;
      replace S: feh, nitrogen with S: feh | nitrogen accordingly
    - Update manpage version and remove reference to non-existent Texinfo
      manual (Closes: #893725)
    - Switch to Ayatana indicators by default, patch by Unit 193
      Update indicator recommends to gir1.2-ayatanaappindicator3-0.1 |
      gir1.2-appindicator3-0.1 accordingly
    - Suggest gnome-shell-extension-appindicator over
      gnome-shell-extension-top-icons-plus
  * Update my email.
  * Bump to debhelper 11 and Standards-Version 4.1.3 (no changes needed)
  * Move Vcs-* fields from Alioth to salsa.debian.org
  * Update long description to match updated Suggests.
  * Move debian/watch to GitHub, where the upstream code is now hosted.

 -- James Lu <james@overdrivenetworks.com>  Sat, 31 Mar 2018 10:51:22 -0700

variety (0.6.6-2) unstable; urgency=medium

  * Disable the installation of extra "SSL dependencies" on Debian and Ubuntu.
    This process is fairly unreliable and has both caused false positives
    (LP: #1739944) as well as reports that nothing is solved by the workaround
    (LP: #1577327)

 -- James Lu <bitflip3@gmail.com>  Sat, 10 Mar 2018 11:18:57 -0800

variety (0.6.6-1) unstable; urgency=high

  * New upstream release.
    - Fix shell injection issues via specially crafted filenames in image
      filters, desktop clock, and the "Delete to Trash" option.
    - Fix wallpaper changing support for Plasma 5.
  * Bump Standards-Version to 4.1.0; no changes needed.
  * Drop patches fix-autoscroll-high-cpu.patch and remove-panoramio.patch,
    they were merged upstream.
  * d/copyright: update years, remove CC-BY-SA for the help/
    folder, which was only an unused template removed in 0.6.6.
  * Switch build-dependency from python-imaging to python-pil to match
    dependencies.
  * Recommend python-httplib2: this is used by the QuotesDaddy and
    QuotationPage quote plugins. (LP: #1713723)

 -- James Lu <bitflip3@gmail.com>  Wed, 20 Sep 2017 21:56:38 -0700

variety (0.6.4-3) unstable; urgency=medium

  * Add new patch remove-panoramio.patch to remove Panoramio support
    completely (including unused code and data/ bits). This patch supersedes
    disable-panoramio.patch and dont-embed-underscore.diff
  * Bump Standards-Version to 4.0.0; no changes needed.
  * Add patch remove-outdated-versions-quit.patch: Disable logic bomb based on
    configured "outdated versions" - if this is enabled upstream it will cause
    specific variety versions to fail to start, and this is probably not what
    we want to ship.
  * Remove dependency on deprecated gir1.2-webkit-3.0 - Panoramio was the last
    bit of code to actually use it. (Closes: #866655)

 -- James Lu <bitflip3@gmail.com>  Wed, 05 Jul 2017 12:39:38 -0700

variety (0.6.4-2) unstable; urgency=medium

  * Upload to unstable.
  * Remove recommends on yelp; it doesn't seem to be used anywhere (there is
    no obvious "Help" button anywhere in the menu or the preferences dialog)
  * Add gnome-shell-extension-top-icons-plus & nitrogen to suggests, along
    with a blurb on when/how these are useful. These packages are only needed
    on certain desktops.
  * Demote gir1.2-appindicator3-0.1 to recommends, as it is optional per the
    upstream docs at http://peterlevi.com/variety/how-to-install/
    ("Other distributions/running from source" section)

 -- James Lu <bitflip3@gmail.com>  Mon, 19 Jun 2017 16:52:11 -0700

variety (0.6.4-1) experimental; urgency=low

  * New upstream release.
    - Port from pyexif2 to GExiv2 (LP: #1280349)
  * debian/ folder changes:
    - Refresh dependencies: add python-pil, gir1.2-gexiv2-0.10; remove
      python-imaging, python-pyexiv2
    - Drop desktop-file-keywords.diff, menu-position-varargs.patch, and
      remove-timebombs.patch: they were either merged upstream or superseded.
    - Add python-gi, python-requests, and dh-python to build dependencies to
      fix distutils and debhelper build warnings.
    - Wrap-and-sort dependencies.

 -- James Lu <bitflip3@gmail.com>  Thu, 18 May 2017 18:40:52 -0700

variety (0.6.3-5) unstable; urgency=medium

  * Add fix-autoscroll-high-cpu.patch backported from upstream Bzr revision
    592 (https://bazaar.launchpad.net/~variety/variety/trunk/revision/592)
    This fixes an issue where the Wallpaper Selector and History panels
    drain entire CPU cores when open. (LP: #1494992)
  * Fix list of supported sites in package extended description: Wallbase.cc
    support was removed in 0.6.3, while Wallhaven.cc was never mentioned even
    though it is supported.

 -- James Lu <bitflip3@gmail.com>  Sat, 06 May 2017 16:43:32 -0700

variety (0.6.3-4) unstable; urgency=medium

  * Upload to unstable.
  * Remove python-lxml from build dependencies as well.

 -- James Lu <bitflip3@gmail.com>  Thu, 27 Apr 2017 22:49:27 -0700

variety (0.6.3-3) experimental; urgency=medium

  * d/control: Remove Wallpapers.net from the extended description; it was
    removed upstream in Variety 0.6.1.
  * Remove lxml dependency, it is unused since the Wallpapers.net removal in
    Variety 0.6.1.
  * Drop unused patch setup.py-install-requires.diff
  * Add disable-panoramio.patch from Arch Linux version 0.6.3-3, modifying
    it to also migrate away from Panoramio sources (LP: #1636389)
    - Also remove libjs-underscore dependency, as Panoramio was the only
      source using it.
  * Refresh menu-position-varargs.patch.
  * Refresh forwarded state & dates of most patches.

 -- James Lu <bitflip3@gmail.com>  Mon, 17 Apr 2017 11:13:39 -0700

variety (0.6.3-2) experimental; urgency=medium

  * Add menu-position-varargs.patch from upstream Bzr revision 582
    (https://bazaar.launchpad.net/~peterlevi/variety/trunk/revision/582)
    to fix menus showing up at the top left of the screen on some systems.
    (LP: #1598298)

 -- James Lu <bitflip3@gmail.com>  Wed, 15 Mar 2017 12:12:09 -0700

variety (0.6.3-1) unstable; urgency=medium

  * New upstream release.
  * Add patch d/patches/remove-timebombs.patch, to remove a fixed 180-day
    timebomb introduced upstream.
  * debian changes:
    - Switch Vcs-Browser field to cgit.
    - Fix Vcs-Git URL.

 -- James Lu <bitflip3@gmail.com>  Wed, 09 Nov 2016 20:55:55 -0800

variety (0.6.2-1) unstable; urgency=medium

  * New upstream release.
  * Merge upstream packaging changes:
    - Add depends on python-requests and python-imaging, removing python-pil.
    - Suggest feh for i3-wm support (LP: #1584748)
  * debian/ changes:
    - Bump Standards Version to 3.9.8 - no changes needed.
    - Fix python-distutils-extra build-dep version (should be >= 2.18, not >=
      2.10) (LP: #1368424)
    - Update watch file for new tarball version format used by upstream.
    - Override script-not-executable Lintian warning for install_ssl_deps.sh
      (not directly executed by the user).
    - Use https:// links in Vcs-* fields.

 -- James Lu <bitflip3@gmail.com>  Mon, 29 Aug 2016 17:07:51 -0700

variety (0.6.0-1) unstable; urgency=low

  * Set section to graphics instead of python. (Closes: #814705)
  * New upstream release.

 -- James Lu <bitflip3@gmail.com>  Sun, 21 Feb 2016 18:01:34 -0800

variety (0.5.5-1) unstable; urgency=medium

  * Initial Debian release. (Closes: #758223)

 -- James Lu <bitflip3@gmail.com>  Sun, 08 Nov 2015 10:08:48 -0800
